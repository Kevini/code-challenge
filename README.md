## About Code-Challenge

This is a application built in **[Laravel](https://laravel.com)** backend and **[VueJs](https://vuejs.org/)** framework for the front-end with a featherweight authentication system called Sanctum for token based API authentication:
This code challenge covers the use of :

- CRUD Operations
- Pivot Tables
- API Building 
- API Authentication using Sanctum
- Vue Components, Routes & Axios
- Laravel's Magic 😎

## API Authentication

I am yet to build the front-end login authentication however to test the authentication, copy the routes you wish to authenticate then paste to the sanctum auth group route.

### Installation

- Clone the repo and cd into it
- composer install
- npm install
- npm run dev
- Rename or copy .env.example file to .env
- export the challenge.sql to your DB
- php artisan key:generate
- php artisan serve
- Visit localhost:8000 in your browser

### Contact

Feel free to reach out to me on **[Mail](mailto:kip.kevin.kk@gmail.com?subject=[BitBucket]-Code Challenge)** for any other questions or comments on this project.

## License

The Laravel framework is open-sourced software licensed under the [MIT license](https://opensource.org/licenses/MIT).
