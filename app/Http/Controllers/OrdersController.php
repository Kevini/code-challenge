<?php

namespace App\Http\Controllers;

use App\Models\Order;
use App\Models\Product;
use Illuminate\Http\Request;

class OrdersController extends Controller
{
    public function index(): \Illuminate\Database\Eloquent\Collection|array
    {
        return Order::all();
    }

    public function store(Request $request): \Illuminate\Http\JsonResponse
    {
        $request->validate(['order_number' => 'required']);

        $order = Order::create($request->all());

        $product = Product::latest()->first();

        $order->products()->attach($product);

        return response()->json($order, 201);
    }

    public function show(Order $order)
    {
        return Order::find($order);
    }

    public function update(Order $order, Request $request): \Illuminate\Http\JsonResponse
    {
        $order->update($request->all());

        return response()->json($order, 200);
    }

    public function destroy(Order $order): \Illuminate\Http\JsonResponse
    {

        $order->delete();
        return response()->json(null, 204);
    }

    public function ordersProducts(): \Illuminate\Http\JsonResponse
    {
        $latestOrders = Order::with('products')->get();

        return response()->json($latestOrders, '200');
    }
}
