<?php

namespace App\Http\Controllers;

use App\Models\Order;
use App\Models\Product;
use Illuminate\Http\Request;
use JetBrains\PhpStorm\NoReturn;

class ProductsController extends Controller
{

    public function index(): \Illuminate\Database\Eloquent\Collection|array
    {
        return Product::all();
    }

    public function store(Request $request): \Illuminate\Http\JsonResponse
    {
        $request->validate([
            'name' => 'required',
            'description' => 'required',
            'quantity' => 'required',
        ]);

       Product::create($request->all());

        return response()->json(['message' => 'product added successfully']);
    }


    public function show(Product $product)
    {
        return Product::find($product);
    }

    public function update(Request $request, Product $product): \Illuminate\Http\JsonResponse
    {
        $product->update($request->all());

        return response()->json($product, 200);
    }

    public function destroy(Product $product): \Illuminate\Http\JsonResponse
    {
        $product->delete();

        return response()->json(['message' => 'Product Deleted'], 204);
    }


}
