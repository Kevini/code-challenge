<?php

namespace App\Http\Controllers;

use App\Models\Product;
use App\Models\Supplier;
use Illuminate\Http\Request;

class SuppliersController extends Controller
{

    public function index(): \Illuminate\Database\Eloquent\Collection|array
    {
        return Supplier::all();
    }

    public function store(Request $request)
    {
        $request->validate(['name' => 'required']);

        $supplier = Supplier::create($request->all());

        $product = Product::latest()->first();

        $supplier->products()->attach($product);

        return response()->json($supplier, 201);
    }


    public function show(Supplier $supplier)
    {
        return Supplier::find($supplier);
    }


    public function update(Request $request, Supplier $supplier)
    {
        $supplier->update($request->all());

        return response()->json($supplier, 200);
    }


    public function destroy(Supplier $supplier)
    {
        $supplier->delete();

        return response()->json(null, 204);
    }

    public function products(): \Illuminate\Http\JsonResponse
    {
        $products = Supplier::with('products')->get();

        return response()->json($products, '200');
    }
}
