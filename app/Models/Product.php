<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Product extends Model
{
    protected $fillable = ['name','description','quantity'];

    use HasFactory;
    use SoftDeletes;

    public function suppliers()
    {
        return $this->hasMany(Supplier::class);
    }

}
