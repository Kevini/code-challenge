import Products from './components/Products.vue';
import CreateProduct from './components/CreateProduct.vue';
import EditProduct from "./components/EditProduct";
import ProductOrders from "./components/ProductOrders";
import Suppliers from "./components/Suppliers";
import CreateOrder from "./components/CreateOrder";
import EditOrder from "./components/EditOrder";
import CreateSupplier from "./components/CreateSupplier";
import EditSupplier from "./components/EditSupplier";

export const routes = [
    {
        name: 'home',
        path: '/',
        component: Products
    },
    {
        name: 'create-product',
        path: '/create',
        component: CreateProduct
    },
    {
        name: 'edit',
        path: '/edit/:id',
        component: EditProduct
    },
    {
        name: 'orders',
        path: '/orders',
        component: ProductOrders
    },
    {
        name: 'suppliers',
        path: '/suppliers',
        component: Suppliers
    },
    {
        name: 'create-supplier',
        path: '/create-supplier',
        component: CreateSupplier
    },
    {
        name: 'edit-supplier',
        path: '/edit/:id',
        component: EditSupplier
    },
    {
        name: 'create-order',
        path: '/create-order',
        component: CreateOrder
    },
    {
        name: 'edit-order',
        path: '/edit/:id',
        component: EditOrder
    },
];
