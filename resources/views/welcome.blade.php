<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Code Challenge</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@400;600;700&display=swap" rel="stylesheet">

        <link href="{{asset("css/app.css")}}" rel="stylesheet">

        <!-- Styles -->
    </head>
    <body class="font-sans text-sm text-gray-900 bg-gray-100">
    <header class="flex items-center justify-between px-8 py-4">

        <a href="/">
            <img src="./logo.png" class="w-44" alt="">
        </a>
        <div>
            <img src="https://www.gravatar.com/avatar/f9879d71855b5ff21e4963273a886bfc?d=mp" alt="avatar" class="w-10 -h-10 rounded-full">
        </div>

    </header>
        <div id = "app"></div>
        <script src="{{asset("js/app.js")}}"></script>
    </body>
</html>
