<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\OrdersController;
use App\Http\Controllers\ProductsController;
use App\Http\Controllers\SuppliersController;
use App\Http\Controllers\UsersController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post("v1/login",[UsersController::class,'index']);

Route::group(['middleware' => 'auth:sanctum'], function(){
  // To securely authenticate the api routes
});
Route::prefix('v1')->group(function (){
    Route::get('orders/latestOrders',[OrdersController::class,'ordersProducts']);
    Route::get('suppliers/products',[SuppliersController::class,'products']);
    Route::resource('orders',OrdersController::class);
    Route::resource('products',ProductsController::class);
    Route::resource('suppliers',SuppliersController::class);
});

