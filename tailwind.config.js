const defaultTheme = require("tailwindcss/defaultTheme");
module.exports = {
    purge: [
        './storage/framework/views/*.php',
        './resources/**/*.blade.php',
        './resources/**/*.vue',
        './resources/**/*js'
    ],
  darkMode: false, // or 'media' or 'class'
  theme: {
    extend: {
        fontFamily : {
            sans:['Open Sans', ...defaultTheme.fontFamily.sans],
        },
        spacing:{
            175:'43.75',
        }
    },
  },

  variants: {
    extend: {},
  },
  plugins: [],
}
